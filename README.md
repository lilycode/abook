# abook

A framework for making good looking books.

The abook framework has three components:

* HTML
* ePub
* Kindle

These are different versions of the CSS tailored for different needs.

## Installation

TBD

## The Components

### The HTML Component

The HTML component contains CSS for making a responsive HTML5 version of a book. It also contains some JS for building menus and navigation.

### The ePub Component

TBD

### The Kindle Component

TBD

## Requirements

TBD
